# Seervision Full Stack Developer Assignment

The goal of this assignment is to evaluate the proficiency as fullstack developer.

For this, you will create a todo list web app, with the following requirements. The expected time you should spend on this task is approximately 2 to 3 hours.

If you have any questions about this assignment, please feel free reach out to your point of contact at Seervision.


## Deliverable

A merge request with your todo list app fulfilling the requirements, including a README with instructions on how to run it.

We do not expect a fully working app, and you will have the opportunity to discuss your work during the assignment chat after you have handed in the assignment. So if you have ideas you did not have time to complete, just write them down and we will talk about it.

## Design

This is base design for your app’s design. You are free to improve it as you want. 

![](./example.png)


## Technologies:
- Frontend: React
- Backend: Scripting language of your choice, we suggest using [Python flask](https://flask.palletsprojects.com/en/1.1.x/)


## Required functionalities:
- Create a task
- Get a task
- Get the list
- Edit a task
- Delete a task
- Mark as finished/completed
